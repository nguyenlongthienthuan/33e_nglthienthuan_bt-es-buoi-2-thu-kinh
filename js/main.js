import { data_glasses,item_glasses,item_virtual,item_info } from "../model/model.js";
let render = (list, item_render, id) => {
    let content_html = ``;
    if (Array.isArray(list)) {
        list.forEach((item) => {
            content_html += item_render(item);
        });
        document.getElementById(id).innerHTML = content_html;
    } else { document.getElementById(id).innerHTML = item_render(list); }
}
render(data_glasses, item_glasses, "vglassesList");
document.querySelectorAll("#vglassesList img").forEach((item, index) => {//chọn kính
    item.addEventListener("click", () => {
        render(data_glasses[index], item_virtual, "avatar");
        render(data_glasses[index],item_info,"glassesInfo");
        document.querySelector(".vglasses__info").style.display="block";
        
        document.querySelectorAll(".btn-warning").forEach((item,index,arr)=>{
            if (arr[0].className.indexOf("active")!==-1){document.querySelector(".vglasses__model img").classList.add("si");}
           else if (arr[1].className.indexOf("active")!==-1){document.querySelector(".vglasses__model img").classList.add("thy");}
           else if (arr[2].className.indexOf("active")!==-1){document.querySelector(".vglasses__model img").classList.add("hiep");}
           
        })
    })
})
document.querySelectorAll(".btn-warning").forEach((item, index) => { //after vs before
    item.addEventListener("click", () => {
        if (index ==3) {
            document.querySelector("#avatar img").style.display = "none";
        } else if(index ==4) {
            document.querySelector("#avatar img").style.display = "block";
        }else if(index ==0) {
            document.querySelector(".vglasses__model").style.backgroundImage = "url('../img/model3.jpg')";
            document.querySelector(".vglasses__right .active").classList.remove("active");
             item.classList.add("active");
             document.getElementById("avatar").innerHTML = "";
        }else if(index ==1) {
            document.querySelector(".vglasses__model").style.backgroundImage = "url('../img/model.jpg')";
            document.querySelector(".vglasses__right .active").classList.remove("active");
             item.classList.add("active");
             document.getElementById("avatar").innerHTML = "";
        }else if(index ==2) {
            document.querySelector(".vglasses__model").style.backgroundImage = "url('../img/model2.jpg')";
            document.querySelector(".vglasses__right .active").classList.remove("active");
             item.classList.add("active");
             document.getElementById("avatar").innerHTML = "";
        }
    })
})